subroutine get_eq_val(text80,k1,k2,string)	 !
      implicit none
      integer:: k, k1, k2, nwrd
      character text80*80, string*80
!------------------------------------------------
!      get the first characters beyond k1 in text80 and 
!     save the column location of the last character into k2. 
!     example: string='-9.007655e-32     ': no space on left 
!          k2: important parameter 
!--------------------------------------------------
      nwrd= 0
      k2=k1
      string(1:80)=' '   ! initialize string 
!-------------- get the first characters beyond column k1-- --
      do k = k1, 80 
!------- characters may be detected and blank may also be detected -------  
         if(nwrd/=0.and.text80(k:k)==' ') exit ! end 
!-----------------------
         if(text80(k:k)/=' ') then ! any character detected 
            nwrd=nwrd+1
            string(nwrd:nwrd)=text80(k:k)  ! no space on left 
            k2=k
         end if
      end do ! k = k1, 80
!------  the first blank in string must appear at the end of characters. -----------------------
      if(nwrd/=index(string,' ')-1) then
         stop ' error: nwrd incorrect /get_eq_val'
      end if
!--------------- numbers not detected -------
      if(nwrd==0) then
         stop ' error:string not detected /get_eq_val'
      end if
!----------------------
    end subroutine get_eq_val   !
