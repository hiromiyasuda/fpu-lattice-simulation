subroutine get_rel(string,sval)	 ! 
      implicit none
      character string*80, zahl*1
      integer:: k, n09, nval, nwrd, nexp, ked, nsca, nvrz, kpnt, kk
      real(8):: sum, sval
!---------------------------------------------------------------
!     convert to real numeric :  -2333.442e-02
!-------------------------- string length without blanks 
      nwrd=len_trim(string)
!------ one of ('e', 'E', 'd', 'D') may be detected: not 2 signs 
      ked=index(string,'e' )+index(string,'E')+index(string,'d' )+index(string,'D' )	 ! location of exponential sign 		
!-----------------
      nexp=0	 ! exponent 	
!------- non exponential ---example: (-2333.442)
      if (ked==0) goto 4000
!------------------------------------
2000  continue
!---------------- sign intialised (+) -----------------
      nvrz=+1
!--------- exponent ----
      do kk=1, nwrd-ked     ! exponent digits (from right)
!------------------------------------
         k=nwrd+1-kk   ! column# of the character (from left)
         zahl=string(k:k)		     ! get the character in column k 
         if (zahl=='+') exit     ! (+) detected and end 
         if (zahl=='-') then		 ! (-) detected
            nvrz=-1			    	 !  reverse the sign 
            exit      !  terminate 
         end if
         call numb(n09,zahl)		     ! convert to integer 0-9
         nexp=nexp+n09*(10**(kk-1))   ! exponent number (integer)
      end do ! kk=1, nwrd-ked
!--------------- exponent number may be negative integer: e-03
      nexp=nexp*nvrz
!--------------------------------------
4000  continue
!----- left part of exponential sign (or blank): -232.675
      if (ked==0) then	        !  no exponetial format 
         ked=index(string,' ')	! column# of the first blank saved in ked
      end if
!-----------------------------
      nvrz=+1		  ! re-initialise the sign 
      sum=0.0		  ! initialise the real numeric 
!------------- locaation of ".":  kpnt
      kpnt=index(string,'.')
!----- examples: "-907654e-02", "-6766" 
      if (kpnt.eq.0) then	  ! there ist no "."
         kpnt=ked		  ! column# of exponential sign (or blank) saved in kpnt
         goto 5000       ! treat as integer in digit
      end if
!-- "-32214." --- 
      if(kpnt==nwrd) goto 5000
!--"-32214.e-09" 
      if(kpnt+1==ked) goto 5000
!---------- examples:  "-45.0"  "-.3210"	"-56." ------------------
!      no exponential expression, there is a "." 
!      blank is located next to the last character:  ked=nwrd+1=index(string, ' ') 
!     right of "." , numeric smaller than 1.0, check between  kpnt & ked column-wise 
!------------------ ------------------------------------------
      if(kpnt==ked) goto 5000   !  no string on right of "." 
      do 6000 k=1, ked-kpnt-1	    ! to right from "." 
      kk=kpnt+k			    	! colum# of the character 
      zahl=string(kk:kk)	        ! get the character 
      call numb(n09,zahl)			! convert to integer 0-9 
      sum=sum+dble(n09)*(dble(10)**dble(-k)) ! real numeric on right of "." : smaller than 1.0 (real numeric) 
6000  continue
!---------so much to format  "*****.9554523" -------------------
!     sum = smaller than 1.0 (real numeric) 
!----- the first character is ".": example ".4523" ---------
      if(kpnt==1) goto 8000	!  no treatment for left of "." 
!---------- "-.9554523"  "-325532.9554523" 
5000  continue
!---------left part of "."  such as  "-325532.*******" 
      do k=1,kpnt-1   ! digit number(left from ".")
         kk=kpnt-k			 ! column# of a character 
         zahl=string(kk:kk)	 ! column# of a character 
         if (zahl=='+') cycle	 ! if (+) detected, terminate 
         if (zahl=='-') then		 ! when (-) detected   
            nvrz=-1			         !  change the sign 
            cycle			     !    end
         end if
         call numb(n09, zahl)		!  convert to integers (0-9)
         sum=sum+dble(n09*(10**(+k-1))) !  real numeric on left of "." 
      end do ! k=1,kpnt-1 
!-----------------------
8000  continue
!------- final numerical data of the exponential string format -----------
      sval = dble(nvrz)*sum*dble(10)**dble(nexp)
!------------------------
      continue
    end subroutine get_rel	 ! 
