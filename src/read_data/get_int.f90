subroutine get_int(string,nval) ! 
      implicit none
      character string*80, zahl*1
      integer:: k, kk, n09,nval, nwrd,nexp, ked,nsca, nvrz
!------------------------------------------------
!        convert string*80 into integer
!--------------------------------------------------
!    impossible !  : -2333.442e-2
!------------ string length excluding blanks ---
      nwrd=len_trim(string)
!---------- check "." : "." is not allowed. -------
      do k=1, nwrd
         if(string(k:k)=='.') then ! check column #
            write(*,*) ' error:	"."	unacceptable/get_int'
            stop
         end if
      end do ! k=1, nwrd
!------------------- check "e"  and "d" 
      nexp=0	! exponent 					  
      ked=0	! location of exponent sign 
!----('e', 'e', 'd', 'd') may be detected: --
      ked=index(string,'e')+index(string,'e')+index(string,'d')+index(string,'d')
!---- if no exponent sign, the location of the first blank will be saved in ked 
      if (ked==0) then
         ked=index(string,' ')	 ! no blanks on left of string
         goto   4000
      end if
!--------- exponential format (exponential sign in ked)--------------
1500  continue
!------ if e-03 : error negative exponent is not allowed 
      do k=ked+1, nwrd
         if(string(k:k)=='-') then
            write(*,*) ' error:(e-03) unacceptable/get_int '
            stop
         end if
      end do ! k=ked+1, nwrd
!---------------------------------------------------
!      "e+03" is acceptable in get_int 
!--- check +03 on right of  exp -------------------
      do kk=1, nwrd-ked	 !	location of exponent from right 
!---------------------------------------------------
         k=nwrd+1-kk	                       ! column# of the character (from left)
         if (string(k:k)=='+') cycle  ! finished to get exp 
!------- (sign may appear, but only (+) ------------------------------
         zahl=string(k:k)              !  get the character in column k 
         call numb(n09, zahl)          ! convert zahl to  0-9 
         nexp=nexp+ n09*(10**(kk-1))	  !	 integer of  exponent (integer numeric)
      end do ! kk=1, nwrd-ked
!--------------- no exponential format --
4000  continue
!----------- check : -2333 without "."  is impossible (human error)------
      nsca=0	  !	 integer on left of exp (numeric)
      nvrz=+1	  !	 sign  
!-------leftward  from e (exp/blank is located in ked)----------
      do kk=1, ked-1	 !  digit# (from right)
         k=ked-kk			 !  column# of the character (from left)
!---------- check the existence of sign (+,-)-------------  
         if (string(k:k)=='+')  cycle  ! (+) detected, finish 
         if (string(k:k)=='-')  then        ! (-) detected 
            nvrz= - 1			     !  chnage the sign 
            cycle 			     !    end 
         end if
!--------------- 1, 10, 100, 1000,... from right (digit for 1) 
         zahl=string(k:k)	           !  column# of the number character(from left)
         call numb(n09,zahl)	           !  convert zahl to 0-9 
         nsca=nsca+ n09*(10**(kk-1))	   !  digit# (from right)   
      end do !  kk=1, ked-1	
!---------------- sign considered -   
      nsca=nvrz*nsca  ! integers on left of exp (numeric)---------------
!------- final integer numeric expressed by exponential examples ----  
      nval=nsca*(10**nexp)
    end subroutine get_int    ! 
