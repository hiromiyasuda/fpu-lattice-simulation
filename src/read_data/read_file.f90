subroutine read_file	!
      use global_variables_mbd
      implicit none
      character text80*80
!------------------------------------------
!   read all lines in inputdata.dat, until(quit) will be detected.
!------------------------------------------------
      write(*,*) ' reading: inputdata.dat'
      call read_line(text80)
!-- --------check the headline with (memo) --------
      if(index(text80,'(memo)')==0) then
         write(*,*) ' error: (memo) missed.' 
         stop
      endif
!------ --------------- read data blocks in sequence 
3110  call data_memo;   if(inwait/=0)  call wait
3120  call data_system;  if(inwait/=0)  call wait
3150  call data_disp;    if(inwait/=0)  call wait
      
!--- - check '(quit)' at the end of input_shell.txt---------------  
      call read_line(text80) ! detect 'quit' 
      if(index(text80,'(quit)')==0) then
           stop ' error: (quit) missed.'
      endif
      write(*,*) ' (quit) detected  and  read_file quit'
    end subroutine read_file   !
