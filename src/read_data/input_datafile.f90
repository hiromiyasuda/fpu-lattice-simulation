subroutine input_datafile ! 
!--------------------------------------------------
!  input data file must be located in file_name='./inputdata.txt'
!----------------------------------------------------------
      call open_file
      call read_file
      call close_file
    end subroutine input_datafile !
