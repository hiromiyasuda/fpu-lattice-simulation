subroutine numb(n09, zahl)	!
!-- ----------------------------------------
!     convert the string  zahl  into integer  n09 
!-- ----------------------------------------------
      implicit none
      integer:: n09
      character zahl*1
      n09= -230241 ! temporary values  
      if (zahl=='0')  n09=0;  if (zahl=='1')  n09=1;  if (zahl=='2')  n09=2; if (zahl=='3')  n09=3
      if (zahl=='4')  n09=4;  if (zahl=='5')  n09=5;  if (zahl=='6')  n09=6; if (zahl=='7')  n09=7
      if (zahl=='8')  n09=8;  if (zahl=='9')  n09=9
!-- -----when convert is impossible --------------
      if (n09==-230241) then
         write(*,*) zahl, ' error: not integer/numb'
         stop
      end if
    end subroutine numb	  !      
