subroutine get_name_eq_val(name,sval,nval) 
!-----------------------------------------------------
!     example:    nabc=-879  &  abcd=-19.77e-3 
!     reads only a single line: nabc=-98707 or bcdf=-0.980e-03
!-----------------------------------------------------
!	   name:  variable name (max 6 characters string)
!	   sval: real type numerics for real type variable  name
!		    	    (invalid, when name is integer type)
!	   nval: integer type numerics for integer type variable name 
!                     (invalid, when name is real type)
!------------------------------------------
      implicit none
      integer::k, intrel, kend, nval, keq, kstart
      real(8):: sval
      character text80*80, string*80, name*6
!----------------------------
1000  call read_line(text80)
!-------- when  end detected, then  name='end'  and exit ----
      if(index(text80,'end')/=0) then
         name='end'	!  doing temporarily 
         return ! goto 9000   ! exit 
      end if
      if(index(text80,'=')==0) then
         stop ' error: "=" missed/get_name_eq_val '
      endif
      keq=index(text80,'=') ! location of "="  
!-------- get the variable name on the left of "=" ----------------
      kstart=keq-1
      call get_name_eq(text80,keq, string)  ! no blanks on the left of string
!----- get the first 6 characters in string*80 and save them into  name*6---------
      do k=1,6
         name(k:k)=string(k:k)
      end do !  k=1,6
!------ check real type or integer type -------------------
      call get_name_typ(name,intrel)
!------- get the numerics on the right of "=" ---- -----
      kstart=keq+1
      call get_eq_val(text80,kstart, kend, string) ! string
!---------- real type?  or integer type ? ----------
      select case (intrel) !
         case (0) !if(intrel==0) 
!=========================================
!              real type variable 
!========= convert string to real type numerics =====
            call get_rel(string, sval)
            write(*,*) name,':=',sval
!--------------------------------     
         case(1) ! if(intrel==1) 
!==============================================
!             integer type variable 
!========= convert string to integer type numerics =========
            call get_int(string, nval)
            write(*,*) name,':=',nval
!---------------------- reading terminated ------------------
         case default 
            write(*,*) name,' error: intrel incorrect/get_name_eq_val'
            stop

      end select
!----------
    end subroutine get_name_eq_val ! format: nabc=-879 & abcd=-19.77e-3  
