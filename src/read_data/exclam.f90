subroutine exclam(text80)  ! replace all characters on right of "!" with blanks
      implicit none
      integer::kex
      character text80*80
!- --- check; "!" is included in text80? ----
      kex =index(text80,'!'); if (kex==0) return 
!--- all blank out all characters from kex to the last     
      text80(kex:80)= ' '
    end subroutine exclam !
