subroutine get_name_eq(text80,keq,string)	!
      implicit none
      character text80*80, string*80
      integer:: nwrd
1234  integer:: k, keq
!--- ---------------------------------------------
!      "=" is in column keq in text80. get the fisrt 6 characters(variable name) on left of "=".
!      and save the variable name into string = 'seven  ': no blank on left of seven.
!----  ----------------------------------------------
      nwrd= 0
      string(1:80)=' '  ! initialise string
!------- get characters(variable name) on left of "=" in text80----------
      do k=1, keq-1
         string(k:k)=text80(k:k)	 ! between blanks 
      end do !  k=1, keq-1
!---  example: string= '    seven     ' : shift the characters to the left -------- 
      string=adjustl(string) ! string= 'seven         '
!----- string length without blanks -------- 
      nwrd=len_trim(string)
!------------------- no variable name detected -----
      if(nwrd==0) then 
         stop ' error: no string detected/get_name_eq'
      end if
!------------------------------
    end subroutine get_name_eq ! 
