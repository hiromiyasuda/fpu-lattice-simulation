subroutine data_disp  !Initial displacement and velocity
use global_variables_mbd
      implicit none 
      integer:: n,m, nd, keq, kstart, kend, nval
      character text80*80, string*80
      real(8):: x, y, z, th, r, ph, sval
!----------- head line with (disp) check --------------
      call read_line(text80)
      if(index(text80,'(disp)')==0) then 
         stop ' error: (disp) missed!'
      endif
      write(*,*) ' (disp) being processed'
      nd=0 ! counter for nodes gelesen 
!------------
      do ! read line til end in (disp)
         call read_line(text80)  ! read the next line after (disp) 
         if(index(text80,'end')/=0) exit ! end detected 
!---------
         if(index(text80,'unit')==0.or.index(text80,'=')==0 ) then 
            stop ' error:"unit=" missed/(disp)'
         endif
!---------  read Displacement-----------
         nd=nd+1	!  counter for nodes gelesen 
         keq=index(text80,'=') ! get the location of "="-------
!--------------------------------------------------
!           unit = unit index  disp vlct :  initial displacement and velocity
!           unit = 40      disp vlct : 0.01  0.0
!--------- get unit index ----------------------------
         kstart=keq+1
         call get_eq_val(text80,kstart, kend , string)
         call get_int(string, nval) ! convert the string to an integer  ---------------------
         if(nval/=nd) then
            stop   ' error: "unit=" unit index# ?/(disp)'
         endif
!!$         write(*,*)  ' reading  unit index =', nval
!---------- get the input data -----------------------------
         keq=index(text80,':') !  get the location of   ":"  
!---------- get initial displacement  ----------  
         kstart=keq+1
         call get_eq_val(text80,kstart, kend , string)      
         call get_rel(string,x) !  convert x to a real type number 
         disp0(nval)=x
!---------- get initial velocity ----------  
         kstart=kend+1
         call get_eq_val(text80,kstart, kend , string)      
         call get_rel(string,y) !  convert y to a real type number 
         vlct0(nval)=y         
!=================	
      end do ! read line til end in (unit) 
!-------------------------------------
      if(nd/=num_unit) then
         stop   ' error: "unit=" not for all units/(disp)'
      end if
!----------------- print angle0 in  input data
3000  printunit: do n=1, num_unit
         write(*,3100)  n, disp0(n) ,vlct0(n)
3100     format(2x,' unit:', i10,' code inputdata :', E13.5, E13.5)
         if(inwait/=0.and.mod(n,20)==0) call wait
      enddo printunit

    end subroutine data_disp		! (disp) block
