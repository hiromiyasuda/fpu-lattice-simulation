subroutine data_system !  (system) control parameters 
      use global_variables_mbd
      implicit none
      integer:: n, insysk, keq, nwrd, nval, idele, kstart, intrel, k, kend
      character text80*80, string*80, name*9
      real(8):: sval
!----------------
      call read_line(text80) ! get headline (system)
      if(index(text80,'(system)')==0) then 
         stop 'error: (system) missed!'
      endif
      write(*,*) ' (system) being processed '
      inwait    = 0    ! default:  scrolling not paused when reading the input file
      zero      = 1.0d-15   ! deafult: computational zero epsilon
!----------
      do ! read line til end in (system)
         call read_line(text80)
         if(index(text80,'end')/=0) exit ! end detected 
         if(index(text80,'=')==0) then
            stop 'error: "=" missed/(system) '
         endif
!---------- position of " = " -----
         keq=index(text80,'=')  ! "=" detected in text80
         string(1:80)=' '  ! initialised
!=========  getting  Input type in  "input_type="----------------
         if(index(text80,'input_type')/=0) then	  !  'input_type=' detected  
            do k=keq+1, 80	!  get Input type beyond  "="  
               string(k:k)=text80(k:k)	!  space may be included 
            enddo !k=keq+1, 80
            string=adjustl(string)	!  to the left 
            nwrd=len_trim(string)  ! cont the characters without spaces 
!------------- 6 characters alyways in input_type ----
            if(nwrd/=6) then
               stop ' error: input_type not 6 characters /(system)'
            end if
            do  k=1,6 !  always 6 characters
               input_type(k:k)=string(k:k)
            end do ! k=1,6
            write(*,*) ' input_type := ', input_type
            idele=1  
            cycle
         endif
!=========  getting  ODE solver type in  "ode_solver="----------------
         if(index(text80,'ode_solver')/=0) then	  !  'ode_solver=' detected  
            do k=keq+1, 80	!  get ODE solver type beyond  "="  
               string(k:k)=text80(k:k)	!  space may be included 
            enddo !k=keq+1, 80
            string=adjustl(string)	!  to the left 
            nwrd=len_trim(string)  ! cont the characters without spaces 
!------------- 5 characters alyways in ode_solver ----
            if(nwrd/=5) then
               stop ' error: ode_solver not 5 characters /(system)'
            end if
            do  k=1,5 !  always 5 characters
               ode_solver(k:k)=string(k:k)
            end do ! k=1,5
            write(*,*) ' ode_solver := ', ode_solver
            idele=1  
            cycle
         endif
!=========  getting  Boundary condition in  "bc_type="----------------
         if(index(text80,'bc_type')/=0) then	  !  'bc_type=' detected  
            do k=keq+1, 80	!  get ODE solver type beyond  "="  
               string(k:k)=text80(k:k)	!  space may be included 
            enddo !k=keq+1, 80
            string=adjustl(string)	!  to the left 
            nwrd=len_trim(string)  ! cont the characters without spaces 
!------------- 5 characters alyways in ode_solver ----
            if(nwrd/=5) then
               stop ' error: bc_type not 5 characters /(system)'
            end if
            do  k=1,5 !  always 5 characters
               bc_type(k:k)=string(k:k)
            end do ! k=1,5
            write(*,*) ' bc_type := ', bc_type
            idele=1  
            cycle
         endif
!-----------! push the string to the left ----------------------------
         call get_name_eq(text80,keq, string) ! get variable characters left side = 
!----- get the first 9 characters in string*80 and put into  name*6---------
         do k=1,9 !  get the first 9 characters in string*80 
            name(k:k)=string(k:k)
         enddo         
!------ real type variable or integer type variable --------------
         call get_name_typ(name,intrel)
!------- get the real number beynd "=" ----------
         kstart=keq+1
         call get_eq_val(text80,kstart, kend, string) ! push string to the left 
!----------real type variable ? or integer type variable ? -------------
         select case(intrel) ! real? or integer?
            case(0) ! intrel=0 real type -
               call get_rel(string,sval) ! convert string to real number 
               if(name=='zero  ') then
                  zero=sval ! real type variables resistered in data_system
               else if(name=='weight') then
                  weight   = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='damp_c') then
                  damp_c   = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='h0_lttc') then
                  h0_lttc = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='spc1') then
                  spc1  = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='spc2') then
                  spc2  = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='spc3') then
                  spc3  = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='force_ex') then
                  force_ex  = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='t_force') then
                  t_force  = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='amp_dis') then
                  amp_dis = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='freq_dis') then
                  freq_dis = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='ext_amp') then
                  ext_amp = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='strn_max') then
                  strn_max = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='tstart  ') then
                  tstart = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='tfinish  ') then
                  tfinish = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='tstep  ') then
                  tstep = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='tsave  ') then
                  tsave = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='epsabs') then
                  epsabs = sval
                  write(*,*) '  ', name, ' := ', sval
               else if(name=='epsrel') then
                  epsrel = sval
                  write(*,*) '  ', name, ' := ', sval
               else
                  write(*,*) '  Error: Import unkown parameter',name,' := ',sval                  
               end if

               
            case(1) !intrel=1  integer type 
               call get_int(string, nval) ! convert string to integer type number 
               if(name=='num_unit') then
                  num_unit  = nval
                  write(*,*) '  ',name,' := ',nval      
               else if(name=='num_sec') then
                  num_sec  = nval
                  write(*,*) '  ',name,' := ',nval      
               else if(name=='i_save ') then
                  i_save  = nval
                  write(*,*) '  ',name,' := ',nval      
               else if(name=='i_tcount ') then
                  i_tcount  = nval
                  write(*,*) '  ',name,' := ',nval
               else
                  write(*,*) '  Error: Import unkown parameter',name,' := ',nval
               end if
               
            case  default ! error
               write(*,*) '  ',name,' error:intrel incorrect/data_system'
               stop
         end select ! select case(intrel)
!------------------------------------------------
      end do ! continue to read lines til end detected in (system) 
      ! Allocate the array
      allocate(disp0(num_unit));disp0=0.D0
      allocate(vlct0(num_unit));vlct0=0.D0
!-----------------------
    end subroutine data_system	  ! control parameters for all job 
