!*****************************************************
!  Wave propagation in FPUT lattice (Hiromi Yasuda)
!*****************************************************
module mbd_code
use iso_c_binding

contains
subroutine main() bind(C,name="main")
      use global_variables_mbd
      implicit none

      write(*,*) '==========================================  '  
      write(*,*) '   FPUT lattice  by Hiromi Yasuda         '   
      write(*,*) '==========================================  '
      write(*,*) ' step01:  read        inputdata.dat  '
      write(*,*) ' step02:  start analysis        '
      write(*,*) ' step03:  terminate'	
      write(*,*) '==========================================  '

1000  write(*,*) 'step01. open     inputdata.dat  ' 
      call input_datafile
      write(*,*) 'step01. closed   inputdata.dat'

2000  write(*,*) 'step02. start analysis. ' 
      !==================================================
      ! Solver
      !==================================================
      call ode_initval_driver
3000  write(*,*) 'step03. terminate '
       ! Deallocate array
      deallocate(disp0)
      deallocate(vlct0)
    end subroutine main
  end module mbd_code




	  
