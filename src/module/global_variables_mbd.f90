module global_variables_mbd ! global variables registered 
      implicit none   
      character  input_type*6 !Input type
      character ode_solver*5 !ODE solver type
      character bc_type*5 !Boundary condition
      integer :: num_unit
      integer :: num_sec
      real(8) :: weight
      real(8) :: damp_c
      real(8) :: h0_lttc
      real(8) :: spc1
      real(8) :: spc2
      real(8) :: spc3
      real(8) :: force_ex
      real(8) :: t_force
      real(8) :: torq_ex
      real(8) :: t_torq
      real(8) :: amp_dis
      real(8) :: freq_dis
      real(8) :: amp_ang
      real(8) :: freq_ang
      real(8) :: ext_amp
      real(8) :: strn_max
      real(8) :: tstart
      real(8) :: tfinish
      real(8) :: tstep
      real(8) :: tsave
      real(8) :: epsabs
      real(8) :: epsrel
      integer :: i_save
      integer :: i_tcount
      integer :: inwait
      real(8) :: zero
      real(8),dimension(:), allocatable :: disp0
      real(8),dimension(:), allocatable :: vlct0
      real(8),dimension(:), allocatable :: disp1
      real(8),dimension(:), allocatable :: rang1
!!$      real(8),dimension(:), allocatable :: q_var
!!$      real(8),dimension(:), allocatable :: dq_var
!!$      real(8),dimension(:), allocatable :: ddq_var
!!$      integer,dimension(:), allocatable :: seed

! ------------------------(quit)
    end  module global_variables_mbd
