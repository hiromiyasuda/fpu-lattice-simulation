!include "config.h"
module mod_ode
  use fgsl
  use global_variables_mbd
  use, intrinsic :: iso_c_binding
  implicit none
contains
  
  function func(t, y_c, dydt_c, params) bind(c)
    real(c_double), value :: t
    type(c_ptr), value :: y_c, dydt_c
    real(c_double), dimension(:), pointer :: y, dydt
    type(c_ptr), value :: params
    integer(c_int) :: func
    real(8) :: force, f_ex, x
    real(8) :: cn_mass,pi,wvw_inv,wvspeed,alpha,beta,t0,c0
    integer :: i,i0
    ! Interpolation
    type(fgsl_interp_accel) :: acc
    type(fgsl_spline) :: spline
    integer(fgsl_size_t) :: n_tdata1
    integer(fgsl_int) :: status

    pi = 4.D0*atan(1.D0)
    call c_f_pointer(y_c, y, [2*num_unit])
    call c_f_pointer(dydt_c, dydt, [2*num_unit])

    force  = 0.D0
    !---External force
    if (t<=t_force) then
       f_ex = force_ex
    else
       f_ex = 0.D0
    end if
    !---Use KdV soluton solution
    if (input_type=='KDVSOL') then
       alpha = spc1/weight
       beta  = spc2/weight
       c0    = sqrt(alpha)
       wvw_inv = sqrt(2.D0*beta*strn_max*h0_lttc/alpha)/h0_lttc
       wvspeed = (c0 + beta*(strn_max*h0_lttc)/(3.D0*c0))*h0_lttc
       t0    = 5.D0/(wvw_inv*wvspeed)
       y(1)  = (strn_max)*(1.D0/wvw_inv)*(Tanh(wvw_inv*wvspeed*(t0-t))+1.D0)
       y(num_unit+1) = -1.D0*(strn_max)*wvspeed*(1./Cosh(wvw_inv*wvspeed*(t0-t))+1.D0)**2
    end if
    ! Initialize
    cn_mass = 1.D0/weight
    dydt(1:num_unit) = y(num_unit+1:2*num_unit)
    !Equation of motion
    i0 = num_unit
    do i=1,num_unit
       ! First unit cell -------------------------------------
       if(i==1) then
          call Force_calculation(t,1,y(1)-y(2),force)
          select case(input_type)
          case('IMPACT')
             dydt(i0+i) = cn_mass*(-force+f_ex) - damp_c*dydt(i)
          case('SINEXC')
             dydt(i0+i) = cn_mass*(-force  + amp_dis*sin(2.D0*pi*freq_dis*t)) - damp_c*dydt(i)
          case('KDVSOL')
             dydt(i0+i) = cn_mass*(-force) - damp_c*dydt(i)
          case default
             dydt(i0+i) = cn_mass*(-force) - damp_c*dydt(i)
          end select
          if(bc_type=='PERDC') then
             call Force_calculation(t,i-1,y(num_unit-1)-y(num_unit),force)
             dydt(i0+i) = dydt(i0+i) + cn_mass*force
          end if
       ! Last unit cell --------------------------------------
       else if (i==num_unit .and. i/=1) then
          if(bc_type=='PERDC') then
             call Force_calculation(t,i,y(1)-y(2),force)
             dydt(i0+i) = - cn_mass*force - damp_c*dydt(i)
          else
             call Force_calculation(t,i,y(i),force)
             dydt(i0+i) = - cn_mass*force - damp_c*dydt(i)
          end if
          call Force_calculation(t,num_unit-1,y(num_unit-1)-y(num_unit),force)
          dydt(i0+i) = dydt(i0+i) + cn_mass*force
       ! Other unit cells -----------------------------------
       else
          call Force_calculation(t,i,y(i)-y(i+1),force)
          dydt(i0+i) = - cn_mass*force - damp_c*dydt(i)
          call Force_calculation(t,i-1,y(i-1)-y(i),force)
          dydt(i0+i) = dydt(i0+i) + cn_mass*force
       end if
    end do
    func = fgsl_success
  end function func



  function jac(t, y_c, dfdy_c, dfdt_c, params) bind(c)
    real(c_double), value :: t
    type(c_ptr), value :: y_c, dfdy_c, dfdt_c
    real(c_double), dimension(:), pointer :: y, dfdy, dfdt
    type(c_ptr), value :: params
    integer(c_int) :: jac
!    real(c_double), pointer :: mu

    call c_f_pointer(y_c, y, [2*num_unit])
    call c_f_pointer(dfdy_c, dfdy, [4*num_unit])
    call c_f_pointer(dfdt_c, dfdt, [2*num_unit])

    dfdy = 0.D0
    dfdt = 0.D0

    jac = fgsl_success
  end function jac
end module mod_ode
