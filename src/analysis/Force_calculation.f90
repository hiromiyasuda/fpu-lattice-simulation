subroutine Force_calculation(time,i_unit,u,force)
      use global_variables_mbd
      implicit none
      real(8),intent(in) :: time
      integer,intent(in) :: i_unit
      real(8),intent(in) :: u
      real(8),intent(out) :: force
      
      !---Initial length of part A and B
      force  = spc1*u + spc2*u*u + spc3*u**3.0
    end subroutine Force_calculation
