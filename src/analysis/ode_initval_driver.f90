subroutine ode_initval_driver
  use mod_ode
  use global_variables_mbd
  implicit none
  type(fgsl_odeiv2_system) :: ode_system
  type(fgsl_odeiv2_step) :: ode_step
  type(fgsl_odeiv2_driver) :: ode_d
  type(c_ptr) :: ptr
  integer(fgsl_int) :: status
  integer :: i,it,i_t_save,i_prog
  real(fgsl_double) :: y(2*num_unit), h, t, t1,yerr(2*num_unit),dydt_in(2*num_unit),dydt_out(2*num_unit)
!  real(fgsl_double), target :: mu
  character(kind=fgsl_char, len=fgsl_strmax) :: name
  integer(fgsl_size_t) :: n_dim
  real(8) :: strn_dsp(2*num_unit)
  real(8) :: ti,wvw_inv,wvspeed,alpha,beta,t0,c0
  ! Note: bsimp uses the jacobian as opposed to rk8pd
  n_dim = 2*num_unit
  t  = 0.0_fgsl_double
  t1 = tfinish
  h  = tstep
  select case(ode_solver)
    case('bsimp')
       ode_step   = fgsl_odeiv2_step_alloc(fgsl_odeiv2_step_bsimp,n_dim)
       name       = fgsl_odeiv2_step_name(ode_step)
       ode_system = fgsl_odeiv2_system_init(func,n_dim, ptr, jac)
       ode_d      = fgsl_odeiv2_driver_alloc_y_new(ode_system,fgsl_odeiv2_step_bsimp,h,1.0e-6_fgsl_double,0.0_fgsl_double)
    case('rkf45')
       ode_step   = fgsl_odeiv2_step_alloc(fgsl_odeiv2_step_rkf45,n_dim)
       name       = fgsl_odeiv2_step_name(ode_step)
       ode_system = fgsl_odeiv2_system_init(func, n_dim)
       ode_d      = fgsl_odeiv2_driver_alloc_y_new(ode_system,fgsl_odeiv2_step_rkf45,h,epsabs,epsrel)
  end select
  !
  
  ! Initialization
  y(1:num_unit)          = disp0
  y(num_unit+1:2*num_unit) = vlct0
  if (input_type=='KDVSOL') then
     alpha = spc1/weight
     beta  = spc2/weight
     c0    = sqrt(alpha)
     wvw_inv = sqrt(2.D0*beta*strn_max*h0_lttc/alpha)/h0_lttc
     wvspeed = (c0 + beta*(strn_max*h0_lttc)/(3.D0*c0))*h0_lttc
     t0    = 5.D0/(wvw_inv*wvspeed)
     y(1)  = (strn_max)*(1.D0/wvw_inv)*(Tanh(wvw_inv*wvspeed*(t0-t))+1.D0)
     y(num_unit+1) = -1.D0*(strn_max)*wvspeed*(1./Cosh(wvw_inv*wvspeed*(t0-t))+1.D0)**2
  end if
    
  write(*, '(''# Using the '',A,'' algorithm'')') trim(name)
  ! Save initial data ---------------------------------
  i_t_save = 1              !Counter for data save
  open(51,FILE="Result_disp.csv",STATUS='replace',ACTION='write')
  open(52,FILE="Result_strain.csv",STATUS='replace',ACTION='write')
  open(53,FILE="Result_velocity.csv",STATUS='replace',ACTION='write')
  write(51,*) 'Time(sec),displacement'
  write(52,*) 'Time(sec),strain'
  write(53,*) 'Time(sec),velocity'
  do i=1,num_unit-1
     strn_dsp(i) = y(i) - y(i+1)
  end do
  if(bc_type=='PERDC') then
     strn_dsp(num_unit) = y(num_unit) - y(1)
  else
     strn_dsp(num_unit) = y(num_unit)
  end if
  write(51,'(10000(E20.9e3,","),E20.9e3)') t,(y(i),i=1,num_unit)
  write(52,'(10000(E20.9e3,","),E20.9e3)') t,(strn_dsp(i),i=1,num_unit)
  write(53,'(10000(E20.9e3,","),E20.9e3)') t,(y(num_unit+i),i=1,num_unit)
  !====================================================
  ! Time integration
  !====================================================
  i_prog = i_save/10
  do it=1,i_save-1
     ti     = it*t1/(i_save-1)
     if (MOD(it,i_prog)==0) then
        write(*,*) 'Progress = ',100*it/i_save+1,'[%]'
     end if
     status = fgsl_odeiv2_driver_apply(ode_d,t,ti,y)
     if (status /= FGSL_SUCCESS) then
        write(*,*) 'Ode function failed !!!!!!!!!!!!!!!!!!!!!!!!!!'
        exit
     end if
     ! Save data ---------------------------------------
!     write(6,fmt='(3(1PE15.8,1X))') t, y(1), y(2)
     do i=1,num_unit-1
        strn_dsp(i) = y(i) - y(i+1)
     end do
     if(bc_type=='PERDC') then
        strn_dsp(num_unit) = y(num_unit) - y(1)
     else
        strn_dsp(num_unit) = y(num_unit)
     end if
     write(51,'(10000(E20.9e3,","),E20.9e3)') t,(y(i),i=1,num_unit)
     write(52,'(10000(E20.9e3,","),E20.9e3)') t,(strn_dsp(i),i=1,num_unit)
     write(53,'(10000(E20.9e3,","),E20.9e3)') t,(y(num_unit+i),i=1,num_unit)
  end do
  close(51)
  close(52)
  close(53)
  call fgsl_odeiv2_step_free (ode_step)
  call fgsl_odeiv2_system_free(ode_system)
  call fgsl_odeiv2_driver_free(ode_d)
 
end subroutine ode_initval_driver
