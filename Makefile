UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
  FC    = gfortran
  F_OPT = -O3 -I/usr/local/include/fgsl -L/usr/local/lib -lgsl -lfgsl -lgslcblas
endif
ifeq ($(UNAME_S),Linux)
  FC    = ifort
  F_OPT = -lmkl_rt -O3 -I/usr/local/include/fgsl -L/usr/local/lib -lgsl -lfgsl -lgslcblas
endif

OBJ_F   = $(wildcard src/module/*.f90)
OBJ_F1  = $(wildcard src/*.f90)
OBJ_F2  = $(wildcard src/read_data/*.f90)
OBJ_F3  = $(wildcard src/analysis/*.f90)

TARGET  = mbd_run.so

$(TARGET):$(OBJ_F)
$(TARGET):$(OBJ_F1)
$(TARGET):$(OBJ_F2)
$(TARGET):$(OBJ_F3)
	$(FC) $(F_OPT) -shared -fPIC $(OBJ_F) $(OBJ_F1) $(OBJ_F2) $(OBJ_F3) -o $(TARGET)
