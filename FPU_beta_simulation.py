import numpy as np
import matplotlib.pyplot as plt
import cmath
import scipy.optimize as opt
from scipy.integrate import ode
from scipy.interpolate import interp1d
from matplotlib import animation
from matplotlib.colors import LightSource, Normalize
from ctypes import (CDLL, POINTER, ARRAY, c_void_p,
                    c_int, byref,c_double, c_char,
                    c_char_p, create_string_buffer)
from numpy.ctypeslib import ndpointer
import os
import shutil   #High level file operation
import glob     #Get file list by using wildcard
import time
from scipy import fftpack
from scipy import integrate
import subprocess
#from mayavi import mlab
g=9.80665

# Figure parameters =================================================
# When you insert the figure, you need to make fig height 2
plt.rcParams['font.family']     = 'sans-serif'
plt.rcParams['figure.figsize']  = 8, 6      # (w=3,h=2) multiply by 3
plt.rcParams['font.size']       = 24        # Original fornt size is 8, multipy by above number
#plt.rcParams['text.usetex']     = True
#plt.rcParams['ps.useafm'] = True
#plt.rcParams['pdf.use14corefonts'] = True
#plt.rcParams['text.latex.preamble'] = '\usepackage{sfmath}'
plt.rcParams['lines.linewidth'] = 3.   
plt.rcParams['lines.markersize'] = 8. 
plt.rcParams['legend.fontsize'] = 21        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.labelsize'] = 24        # Original fornt size is 8, multipy by above number
plt.rcParams['ytick.labelsize'] = 24        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in' 
plt.rcParams['figure.subplot.left']  = 0.2
#plt.rcParams['figure.subplot.right']  = 0.7
plt.rcParams['figure.subplot.bottom']  = 0.2
plt.rcParams['savefig.dpi']  = 300
#==============================================================================
# Triangulated cylindrical origami
#==============================================================================
class FPUT_Lattice:
    def __init__(self,input_type,n_unit,h0_lttc,k_sp1,k_sp2,k_sp3,weight,damp_c,
                 bc_type,ode_solver,tstart,tfinish,tstep,tsave,epsabs,epsrel,dir_cwd,fname_save):
        # Boundary condition
        self.input_type = input_type
        # Dimension
        self.n_unit  = n_unit
        self.h0_lttc = h0_lttc  #[mm]
        self.k_sp1   = k_sp1
        self.k_sp2   = k_sp2
        self.k_sp3   = k_sp3
        self.weight  = weight
        self.damp_c  = damp_c
        # Boundary condition
        self.bc_type = bc_type
        # Solver
        self.ode_solver   = ode_solver
        # Parameters for simulation
        self.tstart    = tstart         #[sec]
        self.tfinish   = tfinish         #[sec]
        self.tstep     = tstep         #[sec]
        self.tsave     = tsave
        self.epsabs    = epsabs
        self.epsrel    = epsrel
        self.i_save    = int(round(tfinish/tsave)+1)
        self.i_tcount  = int(round(tfinish/tstep)+1)     #Total number of itegration
        self.Nframes   = self.i_save
        # Directory where the calculation results are saved
        self.dir_cwd    = dir_cwd
        self.fname_save = fname_save
        

    def make_inputdata(self,disp0,vlct0):
        data = []
        data.append("(memo)!----------comment use--------------------------------")
        data.append('************************************************************')
        data.append('*     Input data for FPUT lattice   (Hiromi Yasuda)         *')
        data.append('************************************************************')
        data.append("Triangulated cylindrical origami")
        data.append("%d Unit Cells"%self.n_unit)
        data.append("end")

        data.append("(system)!---------------------------------------------------")
        data.append("input_type=%s       !Input Type('IMPACT' or 'SINEXC')"%self.input_type)
        data.append("ode_solver=%s       !ODE Solver Type(GNU Scientific Library)"%self.ode_solver)
        data.append("bc_type=%s       !Boundary Type('FIXED' or 'PERDC')"%self.bc_type)
        data.append("num_unit=%d       !Number of unit"%self.n_unit)
        data.append("spc1=%f        !Spring constant"%self.k_sp1)
        data.append("spc2=%f        !Spring constant"%self.k_sp2)
        data.append("spc3=%f        !Spring constant"%self.k_sp3)
        data.append("h0_lttc=%f        !Lattice spacing"%self.h0_lttc)
        data.append("weight=%f        !Mass"%self.weight)
        data.append("damp_c=%e        !Damping coefficient"%self.damp_c)
        
        if self.input_type=='IMPACT':
            data.append("force_ex=%f        !External force"%self.force_ex)
            data.append("t_force=%f        !Time duration for external force"%self.t_force)
        elif self.input_type=='SINEXC':
            data.append("amp_dis=%f        !Sinosoidal excitation"%self.amp_dis)
            data.append("freq_dis=%f        !Frequency for excitation"%self.freq_dis)
        elif self.input_type=='KDVSOL':
            data.append("strn_max=%f        !Maximum strian"%self.strn_max)
        elif self.input_type=='WHITE':
            data.append("ext_amp=%f        !Amplitude for white noise"%self.ext_amp)

        data.append("tstart=%f      !Simulation start time"%self.tstart)
        data.append("tfinish=%f     !Simulation finish time"%self.tfinish)
        data.append("tstep=%e       !Time step"%self.tstep)
        data.append("tsave=%e       !Time step for data save"%self.tsave)
        data.append("epsabs=%e       !Error tolerance"%self.epsabs)
        data.append("epsrel=%e       !Error tolerance"%self.epsrel)
        data.append("i_save=%d      !Step for data save"%self.i_save)
#        data.append("i_tcount=%d     !Total number of integration"%self.i_tcount)
        data.append("end")

        data.append("(disp)!---------------------------------------------------")
        for i in range(self.n_unit):
            data.append("unit=%d   disp vlct :%f %f"%(i+1,disp0[i],vlct0[i]))
        data.append("end")
        
        data.append("(quit)!==================================")

        #Write data into txt file
        with open('inputdata.dat','w') as f:
            for i in range(len(data)):
                f.write(data[i])
                f.write('\n')
        return()

    def Simulation_FPUT(self,sopath,Input_exct):
        # Initial conditions
        disp0 = np.zeros(self.n_unit)
        vlct0 = np.zeros(self.n_unit)
        if self.input_type=='IMPACT':
            self.force_ex = Input_exct[0]   # [N] impact force to the first unit cell
            self.t_force  = Input_exct[1] #[sec] Time duration of external force
        elif self.input_type=='SINEXC':
            self.amp_dis  = Input_exct[0]
            self.freq_dis = Input_exct[1]
        elif self.input_type=='KDVSOL':
            self.strn_max = Input_exct[0]
        elif self.input_type=='INTCND':
            # Set initial velocity
            disp0[:] = Input_exct[:,0]
            vlct0[:] = Input_exct[:,1]
        
        # Create input data
        print('Make input')
        self.make_inputdata(disp0,vlct0)
        # Delete previous data
        if os.path.exists(self.dir_cwd+'/'+self.fname_save):
            shutil.rmtree(self.dir_cwd+'/'+self.fname_save)
        os.makedirs('%s'%(self.dir_cwd+'/'+self.fname_save))
        # Call Fortran program
        print('Start Fortran program')
        time1 = time.time()
        flib = CDLL(sopath)
        ffem = flib.main
        ffem()
        del flib
        time2 = time.time()
        caltime = time2 - time1
        print('Finish Fortran programs')
        print('Calculation time [sec] = {0}'.format(caltime) )
        # Move result files
        dfile = glob.glob("*.csv")
        for i in range(len(dfile)):
            shutil.move(dfile[i],'%s/%s'%(self.dir_cwd+'/'+self.fname_save,dfile[i]))
        print('Finish simulation')
        return()
    
    def AnalyzeSimulation(self,v_strn,vmin_dis,vmax_dis,
                          fig_flag,interpolation):
        #==============================================================================
        # Import data
        #==============================================================================
        print('Import data')
        # Displacement ang rotational angle
        filename = '%s/Result_disp.csv'%(self.dir_cwd+'/'+self.fname_save)
        fortrandata = np.genfromtxt(filename,delimiter=",",skip_header=1)
        # Time
        time0 = fortrandata[:,0]
        self.tstart  = time0[0]
        self.tfinish = time0[-1]
        dt = time0[1] - time0[0]
        
        # Displacement
        disp = np.transpose(fortrandata[:,1:self.n_unit+1])
        # Redefine the number of unit cells -----------------
        if self.bc_type=='PERDC':
            self.n_unit = self.n_unit-1
        # Strain -----------------
        filename = '%s/Result_strain.csv'%(self.dir_cwd+'/'+self.fname_save)
        fortrandata = np.genfromtxt(filename,delimiter=",",skip_header=1)
        # Strain (Displacement)
        strn_dis = np.transpose(fortrandata[:,1:self.n_unit+1])
        # Velocity ------------------
        filename = '%s/Result_velocity.csv'%(self.dir_cwd+'/'+self.fname_save)
        fortrandata = np.genfromtxt(filename,delimiter=",",skip_header=1)
        # Velocity
        velo_dis = np.transpose(fortrandata[:,1:self.n_unit+1])
        
        # Create csv files for animation---------------------------------------
        dir_anime = 'Simulation_ParaviewAnimation'
        # Delete previous data
        if os.path.exists(dir_anime):
            shutil.rmtree(dir_anime)
        os.makedirs(dir_anime)
        
        # Create csv files for paraview animation -----------------------------
#        for i in range(len(time0)):
#            f = open('%s/StrainData4Animation_%d.csv'%(dir_anime,i),'w')
#            f.write('Time[s],Unit index[-],Strain[-]\n')
#            for j in range(self.n_unit):
#                f.write("%f, %d, %f\n"%(time0[i],j+1,strn_dis[j,i]/self.h0_lttc))
#        f.close()
        # Create VTK file for the surface plot --------------------------------
#        data_sim = "Strain_disp_sim.csv" 
#        f = open(data_sim,'w')
#        f.write("Unit index, Time [sec], Strain (disp)\n")
#        for i in range(len(time0)):
#            if time0[i] <= self.tfinish:
#                for j in range(self.n_unit):
#                    f.write("%f,%f,%f\n"%(j+1,time0[i],strn_dis[j,i]/self.h0_lttc))
#            else:
#                break
#        f.close()
#        self.csv2vtk(filename=data_sim)
        
        #==============================================================================
        # Energy analysis
        #==============================================================================
        # Kinetic energy
        energy_Kinet = 0.5*self.weight*velo_dis**2
        # Elastic (potential) energy
        energy_Elast = np.zeros((self.n_unit,len(time0)))
        energy_TTL   = np.zeros(len(time0))
        
        for i in range(self.n_unit):
            if i==self.n_unit-1:
                if self.bc_type=='PERDC':
                    disp_rel = disp[i,:] - disp[i+1,:]
                else:
                    disp_rel = disp[i,:]
            else:
                disp_rel = disp[i,:] - disp[i+1,:]
            energy_Elast[i,:] = 0.5*self.k_sp1*disp_rel**2 + (1./3.)*self.k_sp2*disp_rel**3 +\
                                (1./4.)*self.k_sp3*disp_rel**4
        for i in range(len(time0)):
            energy_TTL[i] = sum(energy_Kinet[:,i]) + sum(energy_Elast[:,i])
            
        print('Maximum Energy error = {0:.4e}'.format( (max(energy_TTL)-min(energy_TTL))/np.mean(energy_TTL) ))
        # =====================================================================
        # Wave speed analysis
        # =====================================================================
        t_peak = np.zeros(self.n_unit)
        for i in range(self.n_unit):
            it_peak   = np.argmax(strn_dis[i,:])
            t_peak[i] = time0[it_peak]
        V_wave = self.h0_lttc/np.gradient(t_peak)
        V_sound = self.h0_lttc*np.sqrt(self.k_sp1/self.weight)
        print('Sound speed = {}'.format(V_sound))
        
        #======================================================================
        # Figures
        #======================================================================     
        if fig_flag==True:
            # Energy analylsis ------------------------------------------------
            plt.figure('Total energy')
            plt.plot(time0,energy_TTL)
            plt.xlabel('Time [s]')
            plt.ylabel('Energy')
            plt.ylim(ymin=0)
            
            # Displacement
            plt.figure('Displacement')
            plt.imshow(disp, aspect='auto',cmap=plt.cm.jet,interpolation='none',
                   extent=[self.tstart-0.5*dt,self.tfinish+0.5*dt,self.n_unit-1+0.5,1-1-0.5])
            cbar = plt.colorbar()
            cbar.solids.set_edgecolor("face")
            cbar.set_label('Displacement, $u$')
            plt.xlabel('Time [s]')
            plt.ylabel('Unit Number, $n$')
            plt.tight_layout()
            
            # Strain ----------------------------------------------------------
            i_mid = int(len(time0)/2)
            plt.figure('Strain wave form')
            plt.plot(np.arange(self.n_unit), strn_dis[:,0]/self.h0_lttc, label='Initial')
            plt.plot(np.arange(self.n_unit), strn_dis[:,i_mid]/self.h0_lttc, label='Midpoint')
            plt.plot(np.arange(self.n_unit), strn_dis[:,-1]/self.h0_lttc, label='Final')
            plt.legend(loc='upper right')
            plt.xlabel('Unit index')
            plt.ylabel('Strain, $| \\xi_u |$')
            plt.ylim(ymin=0)
            
            
#            print(self.tfinish+0.5*dt)
            plt.figure('Strain wave',figsize=(14,6))
            plt.subplot(121)
            plt.title('Raw data',fontsize=22)
            plt.imshow(strn_dis[:,:]/self.h0_lttc, aspect='auto',cmap=plt.cm.jet,interpolation='none',
                   extent=[self.tstart-0.5*dt,self.tfinish+0.5*dt,self.n_unit+0.5,1-0.5])
            cbar = plt.colorbar()
            cbar.solids.set_edgecolor("face")
            cbar.set_label('Strain, $| \\xi_u |$')
            plt.xlabel('Time [s]')
            plt.ylabel('Unit Number, $n$')
#            xlim(0,self.tfinish)
#            ylim(self.n_unit,1)
#            xlim(0,0.4)
#            xticks([0,0.1,0.2,0.3,0.4],['0','0.1','0.2','0.3','0.4'])
            plt.subplot(122)        
            plt.title('Simulation',fontsize=22)
            plt.imshow(strn_dis[:,:]/self.h0_lttc, aspect='auto',cmap=plt.cm.Reds,interpolation=interpolation,
                   vmin=0.01, vmax=v_strn,
                   extent=[self.tstart-0.5*dt,self.tfinish+0.5*dt,self.n_unit+0.5,1-0.5])
            cbar = plt.colorbar(ticks=[0,v_strn])
    #        cbar = colorbar()
#            cbar.ax.set_yticklabels(['$0.0$','%1.2f'%(v_strn)])
            cbar.solids.set_edgecolor("face")
            cbar.set_label('Strain, $| \\xi_u |$')
            plt.xlabel('Time [s]')
            plt.ylabel('Unit Number, $n$')
#            xlim(0,self.tfinish)
#            ylim(self.n_unit,1)
    #        xticks([0,0.1,0.2,0.3,0.4],['0','0.1','0.2','0.3','0.4'])
            plt.tight_layout()
            
            # Wave speed ------------------------------------------------
            plt.figure('Wave trajectory')
            plt.plot(t_peak, np.arange(self.n_unit)+1, 'ro-')
            plt.xlabel('Time (s)')
            plt.ylabel('Unit index')
            
            plt.figure('Wave speed')
            plt.axhline(y=V_sound, color='k',linestyle='--')
            plt.plot(t_peak, V_wave, 'ro-')
            plt.xlabel('Time (s)')
            plt.ylabel('Wave speed')
        return(time0,strn_dis)
    
    def csv2vtk(self,filename):
        with open(filename, "r") as f:
            f.readline()
            
            x = []
            y = []
            value = []
            for line0 in f:
                line = line0[0:len(line0)-1]
                item = line.split(",")
                x.append(item[0])
                y.append(item[1])
                value.append(item[2])
        # output
        basename = os.path.splitext(filename)[0]
        with open(basename + ".vtk", "w") as f:
            f.write("# vtk DataFile Version 2.0\n")
            f.write(basename + "\n")
            f.write("ASCII\n")
            f.write("DATASET UNSTRUCTURED_GRID\n")
            f.write("POINTS %d float\n" % len(x))
            num = len(x)
            for i in range(0, num):
                f.write(x[i] + " " + y[i] + " 0\n")
            f.write("CELLS %d %d\n" % (num, 2*num))
            for i in range(0, num):
                f.write("1 %d\n" % i)
            f.write("CELL_TYPES %d\n" % num)
            for i in range(0, num):
                f.write("1\n")
            f.write("POINT_DATA %d\n" % num)
            f.write("SCALARS point_scalars float\n")
            f.write("LOOKUP_TABLE default\n")
            for i in range(0, num):
                f.write(value[i] + "\n")
        return()
    

    
if __name__ == "__main__":
    #==============================================================================
    # Input parameters
    #==============================================================================
    n_unit  = 100
    h0_lttc = 1.
    # Spring constants (F = k_sp1*u + k_sp2*u**2 + k_sp3*u**3)
    k_sp1 = 1.
    k_sp2 = np.sqrt(1./2.)
    k_sp3 = 1.
    # Material property for the disk
    weight = 1. #0.122309391406     #[kg]
    # Damping coefficient
    damp_c = 0. #13.786177988
    #==============================================================================
    # Simulation conditions
    #==============================================================================
    # Input type
    input_type  = 'INTCND' #'IMPACT', 'SINEXC', 'KDVSOL'
    Input_exct  = []
    if input_type=='IMPACT':
        # External force (impact)
        force_ex = -50.   # [N] impact force to the first unit cell
        t_force  = 1e-3 #[sec] Time duration of external force
        Input_exct = [force_ex,t_force]
    elif input_type=='SINEXC':
        # External force (Sinosoidal excitation)
        amp_dis  = 0.1
        freq_dis = 10. # f (Frequency, not an angular frequency)
        Input_exct = [amp_dis,freq_dis]
    elif input_type=='KDVSOL':
        strn_max   = 0.1
        Input_exct = [strn_max]
    elif input_type=='INTCND':
        vlct0      = 0.7 
        Input_exct = np.zeros((n_unit,2)) #Second index: 0=disp0, 1=vlct0
        Input_exct[0,1] = vlct0
    # Boundary condition
    bc_type     = 'FIXED'  # 'FIXED' or 'PERDC'
    # Solver type
    ode_solver  = 'rkf45' #'rkf45', 'bsimp'
    # Parameters for simulation
    tstart    = 0.0             #Simulation start time
    tfinish   = 100.          #Simulation end time
    tstep     = 1e-6           #[sec] Choose 1e-6 at least -5
    tsave     = 1e-1           #Time step for data save -4
    epsabs    = 1e-12   #the local error on each step within an absolute error
    epsrel    = 1e-12   #the local error on each step within an relative error
    # Directory where the calculation results are saved
    dir_cwd    = os.getcwd()
    fname_save = 'Result_FPUT'

    FPUT = FPUT_Lattice(input_type,n_unit,h0_lttc,k_sp1,k_sp2,k_sp3,weight,damp_c,
                        bc_type,ode_solver,tstart,tfinish,tstep,tsave,epsabs,epsrel,dir_cwd,fname_save)
    # Simulation
    FPUT.Simulation_FPUT(sopath = os.path.abspath("mbd_run.so"),Input_exct=Input_exct)
    # Post process ------------------------------------------------------------
    time0,strn = FPUT.AnalyzeSimulation(v_strn=0.1,vmin_dis=-20.,vmax_dis=30.,
                           fig_flag=True,interpolation='none')
                            
    plt.show()