## Simulation tools for wave propagation in FPU lattice
# Requirements
1. Python
2. Fortran compiler


# How to run
1. Use make file to compile the fortran codes
(in terminal) ```make```

2. Run the main python code   
``` python FPU_beta_simulation.py```


# Example
![alt text](FPU_simulation.png)
